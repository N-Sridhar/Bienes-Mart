import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import "firebase/firebase-analytics";

const firebaseConfig = {
  apiKey: "AIzaSyDmD46hdku5pfhS0KRNGCDWhpKO547neME",
  authDomain: "bienes-mart.firebaseapp.com",
  databaseURL: "https://bienes-mart.firebaseio.com",
  projectId: "bienes-mart",
  storageBucket: "bienes-mart.appspot.com",
  messagingSenderId: "511210839316",
  appId: "1:511210839316:web:cc303acd22b26420a33601",
  measurementId: "G-VM6QFLL1ZX",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

const ga = firebase.analytics();

const storage = firebase.storage();

export { db, storage, ga };
